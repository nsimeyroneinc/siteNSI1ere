---
title: Titre 
subtitle: Sous-titre 
author: Nomm, Prénom 
date: 2021 (cc-by) 
output: beamer_presentation 
theme: Madrid 
colortheme: seahorse 
fontsize: 10pt 
aspectratio: 169 
linkcolor: red
---

# 
# Slide 1

Texte avec liste :

1. un
1. deux
	- un
	- deux
1. trois

# Slide 2

Slide avec deux cadres et une pause entre les deux :

## cadre un

avec du texte

. . .

## cadre deux

avec du texte

----

Slide 3 sans bandeau, avec un saut de ligne et avec une image rÃ©duite Ã  50 %
